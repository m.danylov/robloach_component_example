This is the template to use as component for composer-combining project.
  If a package matches the next rules, it will installed in
separated web-accesible (assets) folder. The default folder name is 'components'
under site root.
  All setup you should make in composer.json file.
Take in consider of the next parts of composer.json of a component package:
  - "type" - must be "component";
  - "require" - should contains "robloach/component-installer",
    it could be in the "root" composer.json only in any case;
  - "extra"->"component" - list of files which we need to copy to assets folder.
    Additionaly, files under "scripts" and "styles" will be concatenated
    together in a two additional files. This is for some optimization.   
  
  Component installer with its dependencies is about 5.6 Mb.
See details about component intaller:
https://packagist.org/packages/robloach/component-installer

  Alternatively usage of component-instaler is configure root composer.json.
In this case we get ability to use any composer/vcs/etc files that have not
configured as a 'component'.
See the example in root-example-composer.json.
